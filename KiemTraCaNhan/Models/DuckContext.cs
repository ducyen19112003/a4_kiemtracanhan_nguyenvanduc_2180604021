﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using Microsoft.EntityFrameworkCore;
using KiemTraCaNhan.Models;

namespace KiemTraCaNhan.Models;

public partial class DuckContext : DbContext
{
    public DuckContext()
    {
    }

   




    public DuckContext(DbContextOptions<DuckContext> options)
        : base(options)
    {
    }


    public DbSet<Accounts> accountss { get; set; }
    public DbSet<Customer> customers { get; set; }
    public DbSet<Employees> employeess { get; set; }
    public DbSet<Logs> logss { get; set; }
    public DbSet<Reports> reportss { get; set; }
    public DbSet<Transactions> transactionss { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=DUCK\\SQLEXPRESS;Database=Duck;Trusted_Connection=True;TrustServerCertificate=true;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
