﻿namespace KiemTraCaNhan.Models
{
    public class Employees
    {
        public int ID {  get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? Contactandaddress {  get; set; }
        public string? Usenameandpassword { get; set; }
    }
}
